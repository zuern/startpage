// Copyright 2023 Kevin Zuern. All rights reserved.

package startpage

import (
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

// Config for the application
type Config struct {
	Nav       []NavLink `yaml:"nav" json:"nav"`
	Locations []string  `yaml:"locations" json:"locations"`
}

// NavLink is a navigation link
type NavLink struct {
	Label string `yaml:"label" json:"label"`
	Class string `yaml:"class" json:"class"`
	URL   string `yaml:"url" json:"url"`
}

// NewConfig loads a config from a YAML file.
func NewConfig(path string) (*Config, error) {
	var cfg Config
	var err error
	defer func() {
		if err != nil {
			err = fmt.Errorf("load config file: %w", err)
		}
	}()
	var cfgBytes []byte
	if cfgBytes, err = ioutil.ReadFile(path); err != nil {
		return nil, err
	}
	if err = yaml.Unmarshal(cfgBytes, &cfg); err != nil {
		return nil, err
	}
	return &cfg, err
}

// Check the configuration
func (c *Config) Check() error {
	var errMsg, m string
	errMsg = "invalid config: %s"
	if len(c.Nav) == 0 {
		return fmt.Errorf(errMsg, "nav has no links")
	}
	for i, n := range c.Nav {
		switch {
		case n.Class == "":
			m = "class"
		case n.Label == "":
			m = "label"
		case n.URL == "":
			m = "url"
		}
		if m != "" {
			m = fmt.Sprintf("nav[%d]: %s not set", i, m)
			return fmt.Errorf(errMsg, m)
		}
	}
	return nil
}
