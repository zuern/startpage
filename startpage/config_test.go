// Copyright 2023 Kevin Zuern. All rights reserved.

package startpage_test

import (
	"testing"

	"gitlab.com/zuern/startpage/startpage"
)

func TestSampleConfigIsValid(t *testing.T) {
	const configFile = "../config-sample.yml"

	cfg, err := startpage.NewConfig(configFile)
	if err != nil {
		t.Fatal(err)
	}

	err = cfg.Check()
	if err != nil {
		t.Fatal("sample configuration is invalid: %w", err)
	}
}
