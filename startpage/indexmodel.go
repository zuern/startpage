// Copyright 2023 Kevin Zuern. All rights reserved.

package startpage

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"sync"

	log "github.com/sirupsen/logrus"
	weather "gitlab.com/zuern/envcanadaforecast"
	"gitlab.com/zuern/envcanadaforecast/types"
)

const xkcd = "https://xkcd.com/info.0.json"

// Data model for startpage.
type IndexModel struct {
	NavLinks []NavLink
	XKCD     map[string]interface{}
	Weather  []*Weather
	wc       *weather.Client
}

// Weather defines the data used for weather info in the startpage.
type Weather struct {
	CurrentConditions CurrentConditions
	Forecast          Forecast
	Location          string
}

// CurrentConditions at a location.
type CurrentConditions struct {
	Status             string
	TemperatureCelcius float64
}

// Forecast.
type Forecast struct {
	Summary string
}

func newIndexModel(cx context.Context, cfg *Config) (m *IndexModel) {
	m = &IndexModel{NavLinks: cfg.Nav, wc: weather.NewClient()}
	wg := &sync.WaitGroup{}
	m.getXKCD(cx, wg)
	weatherChan := make(chan *Weather, len(cfg.Locations))
	for _, loc := range cfg.Locations {
		m.getWeather(cx, wg, loc, weatherChan)
	}
	forecasts := map[string]*Weather{}
	for range cfg.Locations {
		if w := <-weatherChan; w != nil {
			forecasts[w.Location] = w
		}
	}
	wg.Wait()
	close(weatherChan)
	for _, loc := range cfg.Locations {
		if forecast, ok := forecasts[loc]; ok {
			m.Weather = append(m.Weather, forecast)
		}
	}
	return
}

func (m *IndexModel) getXKCD(cx context.Context, wg *sync.WaitGroup) {
	wg.Add(1)
	go func() {
		defer wg.Done()
		log.Debugln("getting newest xkcd")
		req, err := http.NewRequestWithContext(cx, http.MethodGet, xkcd, nil)
		if err != nil {
			log.Errorln(err)
			return
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			log.Errorln(err)
			return
		}
		if resp.StatusCode != 200 {
			log.WithField("code", resp.StatusCode).Error("xkcd fetch failed")
			return
		}
		defer func() { _ = resp.Body.Close() }()
		var bytes []byte
		if bytes, err = ioutil.ReadAll(resp.Body); err == nil {
			err = json.Unmarshal(bytes, &m.XKCD)
		}
		if err != nil {
			log.Error(err)
		}
	}()
}

// getWeather information for the given location loc. loc must be a key in types.LocMap.
func (m *IndexModel) getWeather(cx context.Context, wg *sync.WaitGroup, loc string, weather chan *Weather) {
	wg.Add(1)
	go func() {
		defer wg.Done()
		ctx := log.Fields{"location": loc}
		log.WithFields(ctx).Debugf("getting forecast")
		f, err := m.wc.GetWeather(cx, types.LocMap[loc], false)
		if err != nil {
			log.WithFields(ctx).Error(err)
			weather <- nil
			return
		}
		weather <- &Weather{
			Location: loc,
			CurrentConditions: CurrentConditions{
				Status:             f.CurrentConditions.Condition,
				TemperatureCelcius: f.CurrentConditions.Temperature.Value,
			},
			Forecast: Forecast{
				Summary: f.ForecastGroup.Forecasts[0].TextSummary,
			},
		}
	}()
}
