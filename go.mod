module gitlab.com/zuern/startpage

go 1.13

require (
	github.com/gorilla/mux v1.8.0
	github.com/sirupsen/logrus v1.9.0
	gitlab.com/zuern/envcanadaforecast v0.1.0
	golang.org/x/sys v0.5.0 // indirect
	golang.org/x/text v0.7.0 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
